import unittest
import numpy
import penne

def generate(shape, dtype=numpy.float):
    na = numpy.random.uniform(0., 1., shape).astype(dtype)
    pa = penne.parameter(na)
    return na, pa

def check(na, pa):
    pr = penne.sum(pa * penne.constant(numpy.random.uniform(0., 1., na.shape).astype(na.dtype)))

    result = True
    values = penne.compute_values(pr)
    result = result and numpy.allclose(na, values[pa])

    auto_grad = penne.compute_gradients(pr, values)
    slow_grad = penne.check_gradients(pr, delta=0.1)
    error = 0.
    n = 0
    for subx in slow_grad:
        diff = auto_grad[subx] - slow_grad[subx]
        error += numpy.dot(diff.reshape((-1,)), diff.reshape((-1,)))
        n += diff.size
    error /= n
    result = result and error < 0.01

    return result

class ReductionTestCase(unittest.TestCase):
    def test_sum(self):
        dtype = numpy.float32
        na = numpy.random.uniform(0., 10., (2,3,4)).astype(dtype)
        pa = penne.parameter(na)

        self.assertTrue(check(numpy.sum(na), penne.sum(pa)))
        self.assertTrue(check(numpy.sum(na, axis=0), penne.sum(pa, axis=0)))
        self.assertTrue(check(numpy.sum(na, axis=(0,2)), penne.sum(pa, axis=(0,2))))
        self.assertTrue(check(numpy.sum(na, axis=0, keepdims=True), penne.sum(pa, axis=0, keepdims=True)))
        self.assertTrue(check(numpy.sum(na, axis=(0,2), keepdims=True), penne.sum(pa, axis=(0,2), keepdims=True)))

class EinSumTestCase(unittest.TestCase):
    def test_einsum(self):
        x = penne.parameter(numpy.random.uniform(-1., 1., (10, 20)))
        y = penne.parameter(numpy.random.uniform(-1., 1., (10, 20)))

        o = penne.einsum("ij,ij->", x, y)
        
        auto = penne.compute_gradients(o)
        diff = penne.check_gradients(o, delta=0.1)
        
        error = 0.
        n = 0
        for param in [x, y]:
            d = auto[param]-diff[param]
            error += numpy.sum(d*d)
            n += d.size
        self.assertTrue(error/n < 0.1)

class ConcatenateTestCase(unittest.TestCase):
    def test_concatenate(self):
        na, pa = generate((10, 20))
        nb, pb = generate((10, 30))
        nc = numpy.concatenate([na, nb], axis=1)
        pc = penne.concatenate([pa, pb], axis=1)
        self.assertTrue(check(nc, pc))

cases = [
    ReductionTestCase,
    ConcatenateTestCase,
    EinSumTestCase
]

alltests = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(case) for case in cases])
unittest.TextTestRunner(verbosity=2).run(alltests)
