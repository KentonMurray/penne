"""Recurrent neural networks as finite-state transducers."""

# To do:
# Allow multiple inputs as with make_layer?

from expr import *
from nn import *
import backend as numpy

class Transducer(object):
    """Base class for transducers."""

    def start(self):                   pass
    def start_from(self, other):       pass
    def start_batch(self, size):       pass
    def start_batch_member(self, num): pass

    def transduce(self, inps):
        """Apply transducer to a sequence of input symbols."""

        self.start()
        outputs = []
        for inp in inps:
            outputs.append(self.step(inp))
        return outputs

    def transduce_batch(self, batch, parallel=None):
        """Apply transducer to a batch of sequences of input symbols.

        batch:    A list of lists of input symbols.
        parallel: How many sequences to run in parallel (default all).

        If the sequences are of unequal length, it's better for
        len(batch) to be several times larger than parallel to keep
        all cylinders firing.
        """

        if parallel is None: parallel = len(batch)

        outbatch = [[] for xs in batch]

        # Sort batch by length
        q = range(len(batch))
        q.sort(key=lambda i: len(batch[i]))

        # Set up data structures to keep track of strings to process in parallel
        # If running[num] = [i, j], then next symbol is batch[i][j]
        running = {}
        inp = []
        num = 0
        while len(running) < parallel and len(q) > 0:
            running[num] = [q.pop(), 0]
            inp.append(None)
            num += 1
        self.start_batch(len(inp))

        while len(running) + len(q) > 0:
            for num in xrange(parallel):
                if num not in running: continue
                i, j = running[num]
                if j == len(batch[i]):
                    if len(q) > 0:
                        running[num] = [i, j] = [q.pop(), 0]
                        self.start_batch_member(num)
                    else:
                        del running[num]
                        continue
                inp[num] = batch[i][j]
                running[num][1] += 1
            out = self.step(inp)
            for num, (i, j) in running.iteritems():
                outbatch[i].append(out[num])

        return outbatch

class Map(Transducer):
    """Stateless transducer that just applies a function to every symbol."""

    def __init__(self, f):
        self.f = f
    def step(self, inp):
        return self.f(inp)

class Stack(Transducer):
    """Several stacked recurrent networks, or, the composition of several FSTs."""

    def __init__(self, *layers):
        self.layers = layers

    def start(self):
        for layer in self.layers:
            layer.start()

    def start_batch(self, size):
        for layer in self.layers:
            layer.start_batch(size)
    def start_batch_member(self, number):
        for layer in self.layers:
            layer.start_batch_member(number)

    def start_from(self, other):
        for layer, other_layer in zip(self.layers, other.layers):
            layer.start_from(other_layer)

    def step(self, inp):
        val = inp
        for layer in self.layers:
            val = layer.step(val)
        return val

class Simple(Transducer):
    """Simple (Elman) recurrent network.

    input_dims:  Number of input units.
    output_dims: Number of output units.
    f:           Activation function (default tanh)
    """

    def __init__(self, input_dims, output_dims, f=tanh, model=parameter.all):
        dims = [input_dims, output_dims]
        self.layer = make_layer(dims, output_dims, f=f, model=model)
        self.initial = constant(numpy.zeros((output_dims,))) # or parameter?
        
    def start(self):
        self.state = self.initial
        return self.state

    def start_batch(self, size):
        self.state = stack([self.initial]*size)
    def start_batch_member(self, number):
        self.state = setitem(self.state, number, self.initial)

    def start_from(self, other):
        self.state = other.state
        return self.state

    def step(self, inp):
        """inp can be either a vector Expression or an int """
        self.state = self.layer(inp, self.state)
        return self.state

class GRU(Transducer):
    """Gated recurrent unit.

    Cho et al., 2014. Learning phrase representations using RNN
    encoder-decoder for statistical machine translation. In
    Proc. EMNLP.
    """

    def __init__(self, input_dims, output_dims, model=parameter.all):
        dims = [input_dims, output_dims]
        self.reset_gate = make_layer(dims, output_dims, f=sigmoid, model=model)
        self.update_gate = make_layer(dims, output_dims, f=sigmoid, model=model)
        self.input_layer = make_layer(dims, output_dims, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def start(self):
        self.h = self.h0
        return self.h

    def start_batch(self, size):
        self.h = stack([self.h0]*size)
    def start_batch_member(self, number):
        self.h = setitem(self.h, number, self.h0)

    def start_from(self, other):
        self.h = other.h
        return self.h
    
    def step(self, inp):
        r = self.reset_gate(inp, self.h)
        z = self.update_gate(inp, self.h)
        h_tilde = self.input_layer(inp, r*self.h)
        self.h = (constant(1.)-z)*self.h + z*h_tilde
        return self.h

class LSTM(Transducer):
    """Long short-term memory recurrent network.

    This version is from: Alex Graves, "Generating sequences with
    recurrent neural networks." arXiv:1308.0850.

    input_dims:  Number of input units.
    output_dims: Number of output units.
    f:           Activation function (default tanh)
    """

    def __init__(self, input_dims, output_dims, model=parameter.all):
        dims = [input_dims, output_dims, output_dims]
        self.input_gate = make_layer(dims, output_dims, f=sigmoid, model=model)
        self.forget_gate = make_layer(dims, output_dims, f=sigmoid, bias=5., model=model)
        self.output_gate = make_layer(dims, output_dims, f=sigmoid, model=model)
        self.input_layer = make_layer(dims[:-1], output_dims, f=tanh, model=model)
        self.h0 = constant(numpy.zeros((output_dims,))) # or parameter?
        self.c0 = constant(numpy.zeros((output_dims,))) # or parameter?

    def start(self):
        self.h = self.h0
        self.c = self.c0

    def start_batch(self, size):
        self.h = stack([self.h0]*size)
        self.c = stack([self.c0]*size)
    def start_batch_member(self, number):
        self.h = setitem(self.h, number, self.h0)
        self.c = setitem(self.c, number, self.c0)

    def start_from(self, other):
        self.h = other.h
        self.c = other.c

    def step(self, inp):
        i = self.input_gate(inp, self.h, self.c)
        #f = self.forget_gate(inp, self.h, self.c)
        f = constant(1.) - i
        self.c = f * self.c + i * self.input_layer(inp, self.h)
        o = self.output_gate(inp, self.h, self.c)
        self.h = o * tanh(self.c)
        return self.h
