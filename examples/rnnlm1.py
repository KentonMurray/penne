"""
Deep recurrent language model. This one stacks RNNs using the
recurrent.Stacked class, which is analogous to composition of
finite-state transducers.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time

hidden_dims = 1000
depth = 1

train = lm.read_data("../data/inferno.en")
#train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

valid = lm.read_data("../data/purgatorio.en")
#valid = lm.read_data("../data/ptb.valid.txt")

layers = [recurrent.LSTM(-len(vocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
rnn = recurrent.Stack(*layers)

output_layer = make_layer(hidden_dims, len(vocab), f=logsoftmax)

def make_network(words):
    loss = constant(0.)
    prev_w = numberizer.numberize("<s>")
    rnn.start()
    for word in words:
        w = numberizer.numberize(word)
        o = output_layer(rnn.step(prev_w))
        loss -= o[w]
        prev_w = w
    return loss

trainer = Adagrad(learning_rate=0.1)

prev_clock = time.clock()
for epoch in xrange(100):
    random.shuffle(train)
    train_loss = 0.
    train_size = 0
    for words in train:
        loss = make_network(words)
        train_loss += trainer.receive(loss)
        train_size += len(words)
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for words in valid:
        loss = make_network(words)
        valid_loss += compute_value(loss)
        valid_size += len(words)
    valid_ppl = numpy.exp(valid_loss/valid_size)

    print "epoch=%s walltime=%s train=%s valid=%s" % (epoch, (time.clock()-prev_clock), train_ppl, valid_ppl)
    prev_clock = time.clock()
