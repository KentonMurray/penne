"""RNN encoder-decoder translation model (Sutskever et al., 2014)."""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time

# Read data

data = zip(lm.read_data("../data/inferno.it"),
           lm.read_data("../data/inferno.en"))
random.shuffle(data)
train = data[:-100]
dev = data[-100:]

ftrain, etrain = zip(*train)
fvocab = lm.make_vocab(ftrain, 5000)
fnumberizer = lm.Numberizer(fvocab)
evocab = lm.make_vocab(etrain, 5000)
enumberizer = lm.Numberizer(evocab)

# Model

hidden_dims = 100
depth = 2

## Encoder

layers = [recurrent.LSTM(-len(fvocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
frnn = recurrent.Stack(*layers)

def encode(frnn, fwords):
    frnn.start()
    frnn.step(fnumberizer.numberize("<s>"))
    for f in reversed(fwords):
        frnn.step(fnumberizer.numberize(f))

## Decoder

layers = [recurrent.LSTM(-len(evocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
layers.append(recurrent.Map(make_layer(hidden_dims, len(evocab), f=logsoftmax)))
ernn = recurrent.Stack(*layers)

def decode_loss(frnn, ernn, ewords):
    ernn.start_from(frnn)
    l = constant(0.)
    e_prev = enumberizer.numberize("<s>")
    for e in map(enumberizer.numberize, ewords):
        o = ernn.step(e_prev)
        l -= o[e]
        e_prev = e
    return l

def decode_greedy(frnn, ernn):
    ewords = []
    ernn.start_from(frnn)
    values = {}
    e = enumberizer.numberize("<s>")
    while e != enumberizer.numberize("</s>") and len(ewords) < 100:
        o = ernn.step(e)
        values = compute_values(o, values)
        e = numpy.argmax(values[o])
        ewords.append(e)
    return [enumberizer.w[e] for e in ewords]

# Training

trainer = Adagrad(learning_rate=0.1)

for epoch in xrange(100):
    start_time = time.clock()
    random.shuffle(train)
    train_loss = 0.
    train_n = 0
    for li, (fwords, ewords) in enumerate(train):
        encode(frnn, fwords)
        l = decode_loss(frnn, ernn, ewords)
        train_loss += trainer.receive(l)
        train_n += len(ewords)

    dev_loss = 0.
    dev_n = 0
    for li, (fwords, ewords) in enumerate(dev):
        encode(frnn, fwords)
        l = decode_loss(frnn, ernn, ewords)
        dev_loss += compute_values(l)[l]
        dev_n += 1

        ehyp = decode_greedy(frnn, ernn)
        print "line=%s src=%s" % (li, " ".join(fwords))
        print "line=%s ref=%s" % (li, " ".join(ewords))
        print "line=%s hyp=%s" % (li, " ".join(ehyp))

    print "time=%s epoch=%s train_ppl=%s dev_ppl=%s" % (time.clock()-start_time, epoch, numpy.exp(train_loss/train_n), numpy.exp(dev_loss/dev_n))
