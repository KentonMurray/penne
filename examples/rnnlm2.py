"""
Another implementation of a deep recurrent language model. This one
stacks RNNs by computing the entire output sequence of one RNN before
feeding to the next RNN up.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
from penne import recurrent
import numpy
import random
import time

hidden_dims = 1000
depth = 1

train = lm.read_data("../data/inferno.en")
#train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

valid = lm.read_data("../data/purgatorio.en")
#valid = lm.read_data("../data/ptb.valid.txt")

layers = [recurrent.LSTM(-len(vocab), hidden_dims)]
for i in xrange(depth-1):
    layers.append(recurrent.LSTM(hidden_dims, hidden_dims))
output_layer = make_layer(hidden_dims, len(vocab), f=logsoftmax)

def make_network(words):
    nums = [numberizer.numberize(word) for word in ["<s>"]+words]
    xs = nums[:-1]
    for layer in layers:
        xs = layer.transduce(xs)
    o = output_layer(stack(xs))
    w = stack([one_hot(len(vocab), num) for num in nums[1:]])
    #loss = -einsum("ij,ij->", w, o)
    loss = -dot(reshape(w, (-1,)), reshape(o, (-1,)))
    return loss

trainer = Adagrad(learning_rate=0.1)

prev_clock = time.clock()
for epoch in xrange(100):
    random.shuffle(train)
    train_loss = 0.
    train_size = 0
    for words in train:
        loss = make_network(words)
        train_loss += trainer.receive(loss)
        train_size += len(words)
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for words in valid:
        loss = make_network(words)
        valid_loss += compute_value(loss)
        valid_size += len(words)
    valid_ppl = numpy.exp(valid_loss/valid_size)

    print "epoch=%s walltime=%s train=%s valid=%s" % (epoch, (time.clock()-prev_clock), train_ppl, valid_ppl)
    prev_clock = time.clock()
