"""
Feedforward language model.
Bengio et al., 2003. A neural probabilistic language model. JMLR 3:1137-1155.
"""

import sys
sys.path.append("..")
from penne import *
from penne import lm
import numpy
import random
import time

order = 4
embedding_dims = 1000
hidden_dims = 1000

train = lm.read_data("../data/inferno.en")
#train = lm.read_data("../data/ptb.train.txt")
vocab = lm.make_vocab(train)
numberizer = lm.Numberizer(vocab)

valid = lm.read_data("../data/purgatorio.en")
#valid = lm.read_data("../data/ptb.valid.txt")

input_layer = make_layer(-len(vocab), embedding_dims, f=None, bias=False)
hidden1_layer = make_layer(embedding_dims*(order-1), hidden_dims)
hidden2_layer = make_layer(hidden_dims, embedding_dims)
output_layer = make_layer(embedding_dims, len(vocab))

def logprob(context):
    i = []
    for word in context:
        w = numberizer.numberize(word)
        e = input_layer(w)
        i.append(e)
    h1 = tanh(hidden1_layer(concatenate(i)))
    h2 = tanh(hidden2_layer(h1))
    return logsoftmax(output_layer(h2))

trainer = SGD(learning_rate=0.01)

prev_ppl = None
prev_clock = time.clock()
for epoch in xrange(1):
    random.shuffle(train)
    train_loss = 0.
    train_size = 0
    for ngram in lm.ngrams(train, order):
        p = logprob(ngram[:-1])
        loss = -p[numberizer.numberize(ngram[-1])]
        train_loss += trainer.receive(loss)
        train_size += 1
    train_ppl = numpy.exp(train_loss/train_size)

    valid_loss = 0.
    valid_size = 0
    for ngram in lm.ngrams(valid, order):
        p = logprob(ngram[:-1])
        loss = -p[numberizer.numberize(ngram[-1])]
        valid_loss += compute_value(loss)
        valid_size += 1
    valid_ppl = numpy.exp(valid_loss/valid_size)

    print "epoch=%s walltime=%s train=%s valid=%s" % (epoch, (time.clock()-prev_clock), train_ppl, valid_ppl)
    prev_clock = time.clock()

    if prev_ppl is not None and valid_ppl > prev_ppl:
        trainer.learning_rate /= 2
        print "learning_rate=%s" % trainer.learning_rate
    prev_ppl = valid_ppl
